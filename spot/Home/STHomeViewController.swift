//
//  STHomeViewController.swift
//  spot
//
//  Created by Weslley Madeira on 20/03/19.
//  Copyright © 2019 spot. All rights reserved.
//

import UIKit

class STHomeViewController: UIViewController {
    
    @IBOutlet weak var collectionViewFather: UICollectionView!
    
    var presenter: STHomePresenter!
    var all: [[ImageElement]]?
    var favorite: [[ImageElement]]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configCollectionView()
        
        presenter.viewDidLoad()
        // Do any additional setup after loading the view.
    }



}

extension STHomeViewController: STHomeView {
    func showUser(user: User) {
        if let header =  collectionViewFather.supplementaryView(forElementKind: UICollectionView.elementKindSectionHeader, at: IndexPath(row: 0, section: 0)) as? spot.STHeaderCollectionReusableView {
            header.updateUser(user: user)
        }
    }
    func showCards(elements: [[ImageElement]]) {
        self.all = elements
        self.favorite = elements
        self.collectionViewFather.reloadData()
    }
    
    
}

extension STHomeViewController: UICollectionViewDelegate {
    
    func configCollectionView(){
        self.collectionViewFather.delegate = self
        self.collectionViewFather.dataSource = self
        
        let nibHeader = UINib(nibName: "STHeaderCollectionReusableView", bundle: nil)
        self.collectionViewFather.register(nibHeader, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "HeaderCollectionReusableView" )
        
        let nibCell = UINib(nibName: "STHomeCollectionViewCell", bundle: nil)
        self.collectionViewFather.register(nibCell, forCellWithReuseIdentifier: "HomeCollectionViewCell")
        
    }
    
}

extension STHomeViewController: UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        switch kind {
        case UICollectionView.elementKindSectionHeader:
            let kind = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "HeaderCollectionReusableView", for: indexPath) as! STHeaderCollectionReusableView
            kind.delegate = self
            return kind
        default:
            return UICollectionReusableView()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.row == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionViewCell", for: indexPath) as! STHomeCollectionViewCell
            cell.all = all
            cell.favorite = favorite
            cell.collectionCards.reloadData()
            cell.delegate = self
            return cell
        }else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionViewCell", for: indexPath) as! STHomeCollectionViewCell
            cell.delegate = self
            return cell
        }
        
    }
    
    
}

extension STHomeViewController: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 385)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.row == 0 {
            if let all = all?.count {
                let size = (260 * all) + (8 * (all + 2))
                return CGSize(width: self.view.bounds.width, height: CGFloat(size))
            }
            return CGSize(width: self.view.bounds.width, height: 260)
        }else {
            if let favorite = favorite?.count {
                let size = (260 * favorite) + (8 * (favorite + 2))
                return CGSize(width: self.view.bounds.width, height: CGFloat(size))
            }
            return CGSize(width: self.view.bounds.width, height: 260)
        }
    }
}

extension STHomeViewController: HeaderProtocol{
 
    
    func clicInAll() {
        if  let cell =  self.collectionViewFather.cellForItem(at: IndexPath(row: 0, section: 0)) as? STHomeCollectionViewCell{
            cell.focusFirstView()
        }
    }
    
    func clicInFavorite() {
        if  let cell =  self.collectionViewFather.cellForItem(at: IndexPath(row: 0, section: 0)) as? STHomeCollectionViewCell{
            cell.focusSecondView()
        }
    }
    
    
}

extension STHomeViewController: HomeProtocol{
    func showFirstView() {
        if let header =  collectionViewFather.supplementaryView(forElementKind: UICollectionView.elementKindSectionHeader, at: IndexPath(row: 0, section: 0)) as? spot.STHeaderCollectionReusableView {
            header.allSelected()
        }
    }
    
    func showSecondView() {
        if let header =  collectionViewFather.supplementaryView(forElementKind: UICollectionView.elementKindSectionHeader, at: IndexPath(row: 0, section: 0)) as? spot.STHeaderCollectionReusableView {
            header.favoriteSelected()
        }
    }
    
    
}
