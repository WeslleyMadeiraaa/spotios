//
//  STHomeCardCollectionViewCell.swift
//  spot
//
//  Created by Weslley Madeira on 20/03/19.
//  Copyright © 2019 spot. All rights reserved.
//

import UIKit

class STHomeCardCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var viewBackground: UIView!
    @IBOutlet weak var collectionViewCard: UICollectionView!
    
    var itens: [[ImageElement]]?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.configCollection()
    }
    
    
    func configCollection(){
        collectionViewCard.delegate = self
        collectionViewCard.dataSource = self
        collectionViewCard.isScrollEnabled = false
        let nib = UINib(nibName: "STCardCollectionViewCell", bundle: nil)
        self.collectionViewCard.register(nib, forCellWithReuseIdentifier: "CardCollectionViewCell")
    }
    
}


extension STHomeCardCollectionViewCell: UICollectionViewDelegate {
    
}

extension STHomeCardCollectionViewCell: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return itens?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CardCollectionViewCell", for: indexPath) as! STCardCollectionViewCell
        cell.configAtributs(imagens: self.itens![indexPath.row])
        if indexPath.row == 1 {
            
            cell.imageProfile.image = UIImage(named: "mulherPerfil2")
            cell.labelName.text = "Mariana Rui"
        }
        return cell
    }
    
    
    
    
}

extension STHomeCardCollectionViewCell: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.bounds.width - 32, height: 260)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 8, left: 16, bottom: 8, right: 16)
    }
    
    
    
    
}
