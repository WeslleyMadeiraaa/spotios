//
//  STHomeCollectionViewCell.swift
//  spot
//
//  Created by Weslley Madeira on 20/03/19.
//  Copyright © 2019 spot. All rights reserved.
//

import UIKit

protocol HomeProtocol {
    func showFirstView()
    func showSecondView()
}

class STHomeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var collectionCards: UICollectionView!
    
    var all: [[ImageElement]]?
    var favorite: [[ImageElement]]?
    
    var delegate: HomeProtocol?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.configCollection()
    }
    
    
    func configCollection(){
        let nib = UINib(nibName: "STHomeCardCollectionViewCell", bundle: nil)
        self.collectionCards.register(nib, forCellWithReuseIdentifier: "HomeCardCollectionViewCell")
        self.collectionCards.delegate = self
        self.collectionCards.dataSource = self
        self.collectionCards.isPagingEnabled = true
        
    }
    
    func focusFirstView(){
        self.collectionCards.scrollToItem(at: IndexPath(row: 0, section: 0), at: .left, animated: true)
    }
    func focusSecondView(){
        self.collectionCards.scrollToItem(at: IndexPath(row: 1, section: 0), at: .left, animated: true)
    }
    

}

extension STHomeCollectionViewCell: UICollectionViewDelegate {
    
}

extension STHomeCollectionViewCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCardCollectionViewCell", for: indexPath) as! STHomeCardCollectionViewCell
        cell.itens = all ?? []
        cell.collectionViewCard.reloadData()
        cell.viewBackground.backgroundColor = UIColor.white
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            delegate?.showSecondView()
        }else if indexPath.row == 1 {
            delegate?.showFirstView()
        }
    }

    
    
    
}

extension STHomeCollectionViewCell: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.row == 0 {
            if let all = all?.count {
                let size = (260 * all) + (8 * (all + 2))
                return CGSize(width: self.bounds.width, height: CGFloat(size))
            }
            return CGSize(width: self.bounds.width, height: 260)
        }else {
            if let favorite = favorite?.count {
                let size = (260 * favorite) + (8 * (favorite + 2))
                return CGSize(width: self.bounds.width, height: CGFloat(size))
            }
            return CGSize(width: self.bounds.width, height: 260)
        }
        
    }
    
    
}
