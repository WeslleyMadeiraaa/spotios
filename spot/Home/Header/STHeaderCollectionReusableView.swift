//
//  STHeaderCollectionReusableView.swift
//  spot
//
//  Created by Weslley Madeira on 20/03/19.
//  Copyright © 2019 spot. All rights reserved.
//

import UIKit
import AlamofireImage
import Alamofire

protocol HeaderProtocol{
    func clicInAll()
    func clicInFavorite()
}

class STHeaderCollectionReusableView: UICollectionReusableView {
    
    @IBOutlet weak var imageUser: UIImageView!
    @IBOutlet weak var viewSearch: UIView!
    
    @IBOutlet weak var viewRet: UIView!
    @IBOutlet weak var viewBol: UIView!
    
    @IBOutlet weak var viewBackgroundButton: UIView!
    
    @IBOutlet weak var labelName: UILabel!

    @IBOutlet weak var buttonAll: UIButton!
    @IBOutlet weak var buttonFavo: UIButton!
    
    var delegate: HeaderProtocol?
    var user : User?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.configView()
        
    }
    
    func configView(){
        imageUser.layer.cornerRadius = imageUser.frame.width / 2
        imageUser.clipsToBounds = true
        
        self.viewSearch.layer.borderWidth = 1.2
        
        self.viewSearch.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        self.viewSearch.layer.cornerRadius = 8
        
        self.viewRet.layer.cornerRadius = 8
        self.viewRet.layer.borderWidth = 1.2
        self.viewRet.layer.borderColor = UIColor.blue.cgColor
        self.viewRet.backgroundColor = UIColor.white
        
        self.viewBol.layer.cornerRadius = viewBol.layer.bounds.width / 2
        self.viewBol.layer.borderWidth = 1.2
        self.viewBol.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        self.viewBol.backgroundColor = UIColor.white
        
        viewBackgroundButton.layer.cornerRadius = viewBackgroundButton.layer.bounds.height / 2
        
    }
    
    @IBAction func sendFavorit(_ sender: UIButton){
        self.favoriteSelected()
       
        delegate?.clicInFavorite()
        
    }
    
    @IBAction func sendAll(_ sender: UIButton){
        self.allSelected()
        delegate?.clicInAll()
    }
    
    func favoriteSelected(){
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseInOut, animations: {
            self.viewBackgroundButton.center = self.buttonFavo.center
            self.viewBackgroundButton.bounds = self.buttonFavo.bounds
            self.buttonFavo.setTitleColor(UIColor.white, for: .normal)
            self.buttonAll.setTitleColor(UIColor.lightGray, for: .normal)
            self.layoutIfNeeded()
        }) { (complition) in
            
        }
    }
    
    func allSelected(){
       
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseInOut, animations: {
            self.viewBackgroundButton.center = self.buttonAll.center
            self.viewBackgroundButton.bounds = self.buttonAll.bounds
            self.buttonAll.setTitleColor(UIColor.white, for: .normal)
            self.buttonFavo.setTitleColor(UIColor.lightGray, for: .normal)
            self.layoutIfNeeded()
        }) { (complition) in
            
        }
    }
    
    func sizeAnimation(){
       
    }
    
    func updateUser(user: User){
        self.user = user
        self.requestImage()
        self.labelName.text = user.name
        
    }
    
    func requestImage(){
        if let newUrl = user?.image.url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed){
            Alamofire.request(newUrl).responseImage { (response) in
                if response.data != nil {
                    if let image = response.result.value {
                        self.imageUser.image = image
                    }else {
                        print("error request image")
                    }
                }
            }
        }
    }
    
}
