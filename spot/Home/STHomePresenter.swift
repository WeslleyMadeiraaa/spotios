//
//  STHomePresenter.swift
//  spot
//
//  Created by Weslley Madeira on 20/03/19.
//  Copyright © 2019 spot. All rights reserved.
//

import Foundation
import Alamofire

protocol STHomeView: class {
    func showUser(user: User)
    func showCards(elements: [[ImageElement]])
}

protocol STHomePresenterDelegate {
    func request()
    func viewDidLoad()
}


class STHomePresenter: STHomePresenterDelegate {
    fileprivate weak var view: STHomeView?
    
    
    init(view: STHomeView) {
        self.view = view
    }
    
    func request() {
        STHomeNetworking().request(url: URL(string: "http://api.screengo.com.br/api/v1/tests/list?id=23")!) { (success, error , response)  in
            self.view?.showUser(user: response.user)
            self.configData(data: response)
        }
    }
    
    func viewDidLoad() {
        self.request()
        
    }
    
    func configData(data: STRequestResponse){
        var dict : [Int : [ImageElement]] = [:]
        for image in data.user.screenshots.images {
            if ((dict[image.panelID]) != nil){
                dict[image.panelID]?.append(image)
            }else {
                dict[image.panelID] = [image]
            }
        }
        
        var elements : [[ImageElement]] = []
        for ( _ , array) in dict {
            elements.append(array)
        }
        
        view?.showCards(elements: elements)
        
    }
    
}

class STHomeNetworking {
    
    func request(url: URL , complition: @escaping (_ success: Bool ,_ error: Error? , _ user: STRequestResponse) ->Void){
        Alamofire.request(url).validate().responseData { (data) in
            if data.error == nil{
                do {
                    let response = try JSONDecoder().decode(STRequestResponse.self, from: data.data!)
                    complition(true ,nil , response)
                    print(response)
                }catch{
                    print(error)
                }
            }
        }
    }
}


