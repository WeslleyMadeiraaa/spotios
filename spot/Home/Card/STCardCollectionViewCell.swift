//
//  STCardCollectionViewCell.swift
//  spot
//
//  Created by Weslley Madeira on 20/03/19.
//  Copyright © 2019 spot. All rights reserved.
//

import UIKit
import AlamofireImage
import Alamofire

class STCardCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var viewBackground: UIView!
    
    @IBOutlet weak var imageProfile: UIImageView!
    @IBOutlet weak var firstImage: UIImageView!
    @IBOutlet weak var secondImage: UIImageView!
    @IBOutlet weak var thirdImage: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelPrintCount: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configView()
    }
    
    func configAtributs(imagens: [ImageElement]){
        self.labelPrintCount.text = "\(imagens.count) prints "
        
        if imagens.count > 0{
            self.requestImage(url: imagens[0].image.thumb.url) { (image) in
                self.firstImage.image = image
            }
        }
        if imagens.count > 1{
            self.requestImage(url: imagens[1].image.thumb.url) { (image) in
                self.secondImage.image = image
            }
        }
        if imagens.count > 2{
            self.requestImage(url: imagens[2].image.thumb.url) { (image) in
                self.thirdImage.image = image
            }
        }
        
    }
    
    func configView(){
        self.viewBackground.layer.cornerRadius = 8
        self.viewBackground.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        self.viewBackground.layer.borderWidth = 1.0
        
        imageProfile.layer.cornerRadius = imageProfile.bounds.width / 2
        imageProfile.clipsToBounds = true
        
        let rectShape = CAShapeLayer()
        rectShape.bounds = self.firstImage.frame
        rectShape.position = self.firstImage.center
        rectShape.path = UIBezierPath(roundedRect: self.firstImage.bounds, byRoundingCorners: [.bottomLeft  , .topLeft], cornerRadii: CGSize(width: 6, height: 6)).cgPath
        
        //self.firstImage.layer.backgroundColor = UIColor.green.cgColor
        //Here I'm masking the textView's layer with rectShape layer
        self.firstImage.layer.mask = rectShape
        
        let rectShapeLast = CAShapeLayer()
        rectShapeLast.bounds = self.thirdImage.frame
        rectShapeLast.position = self.thirdImage.center
        rectShapeLast.path = UIBezierPath(roundedRect: self.thirdImage.bounds, byRoundingCorners: [.bottomRight  , .topRight], cornerRadii: CGSize(width: 6, height: 6)).cgPath
        
        //self.firstImage.layer.backgroundColor = UIColor.green.cgColor
        //Here I'm masking the textView's layer with rectShape layer
        self.thirdImage.layer.mask = rectShapeLast
        
        let rectShapeCenter = CAShapeLayer()
        rectShapeCenter.bounds = self.secondImage.frame
        rectShapeCenter.position = self.secondImage.center
        rectShapeCenter.path = UIBezierPath(roundedRect: self.secondImage.bounds, byRoundingCorners: [.bottomRight  , .topRight], cornerRadii: CGSize(width: 0, height: 0)).cgPath
        
        //self.firstImage.layer.backgroundColor = UIColor.green.cgColor
        //Here I'm masking the textView's layer with rectShape layer
        self.secondImage.layer.mask = rectShapeCenter
    }
    func requestImage(url: String?, complition: @escaping (_ image: UIImage) -> Void){
        if let newUrl = url?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed){
            Alamofire.request(newUrl).responseImage { (response) in
                if response.data != nil {
                    if let image = response.result.value {
                        complition(image)
                    }else {
                        print("error request image")
                    }
                }
            }
        }
    }

}
