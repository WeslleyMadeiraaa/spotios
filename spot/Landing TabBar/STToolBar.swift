//
//  STToolBar.swift
//  spot
//
//  Created by Weslley Madeira on 20/03/19.
//  Copyright © 2019 spot. All rights reserved.
//

import Foundation
import UIKit

class STToolbar: UITabBarController {
    
    fileprivate lazy var defaultTabBarHeight = { tabBar.frame.size.height }()
    
    override func viewDidLoad() {
        guard let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView else { return }
        
        statusBar.backgroundColor = #colorLiteral(red: 0.9647058824, green: 0.9647058824, blue: 0.9725490196, alpha: 1)
        
        
        
        self.tabBar.tintColor = UIColor.gray
        let galerry = STHomeViewController()
        let presenterGallerry = STHomePresenter(view: galerry)
        galerry.presenter = presenterGallerry
        galerry.tabBarItem.image = UIImage(named: "iconTabBarGallery")
        galerry.tabBarItem.title = "GALERIA"
        
        let spots = STHomeViewController()
        let presenter = STHomePresenter(view: spots)
        spots.presenter = presenter
        spots.tabBarItem.image = UIImage(named: "iconTabBarSpot")
        spots.tabBarItem.title = "SPOTS"
//
    
        self.viewControllers = [spots , galerry]
        self.tabBar.backgroundColor = UIColor.white
        
        let topBar = CALayer()
        topBar.frame = CGRect(x: 0, y: 0, width: 1000, height: 0.25)
        tabBar.backgroundColor = UIColor.white
        tabBar.layer.addSublayer(topBar)
        tabBar.clipsToBounds = true
        
        
        
        
    }
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        let newTabBarHeight = defaultTabBarHeight + 3
        
        var newFrame = tabBar.frame
        newFrame.size.height = newTabBarHeight
        newFrame.origin.y = view.frame.size.height - newTabBarHeight
        
        tabBar.frame = newFrame
    }
    
}
