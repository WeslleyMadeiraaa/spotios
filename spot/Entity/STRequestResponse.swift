//
//  STRequest.swift
//  spot
//
//  Created by Weslley Madeira on 20/03/19.
//  Copyright © 2019 spot. All rights reserved.
//

import Foundation

struct STRequestResponse: Codable {
    let user: User
}

struct User: Codable {
    let name, email: String
    let image: BigClass
    let screenshots: Screenshots
}

struct BigClass: Codable {
    let url: String
}

struct Screenshots: Codable {
    let qtde: Int
    let images: [ImageElement]
}

struct ImageElement: Codable {
    let id: Int
    let image: PrintImage
    let name: String
    let description: String?
    let panelID, userID: Int
    let createdAt, updatedAt: String
    let privated: Bool
    let detail: String
    let fullPath: String?
    let clarifaiID: String
    let imageProcessed: Bool
    let spotID: String?
    let opened: Bool
    
    enum CodingKeys: String, CodingKey {
        case id, image, name, description
        case panelID = "panel_id"
        case userID = "user_id"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case privated, detail
        case fullPath = "full_path"
        case clarifaiID = "clarifai_id"
        case imageProcessed = "image_processed"
        case spotID = "spot_id"
        case opened
    }
}

struct PrintImage: Codable {
    let url: String
    let thumb, medium, big: BigClass
}
